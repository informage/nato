from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

from ads.models import Ads
from ads.serializers.AdsSerializer import AdvertisementSerializer
from ..presentors.AdsPresentor import AdsPresentor
from ..update_db import delete_old


class MainController(APIView):
    @staticmethod
    def get(request):
        advertisements = Ads.objects.all()
        return Response({"advertisements": AdsPresentor.collection(advertisements)})

    @staticmethod
    def post(request):
        ads = request.data
        serializer = AdvertisementSerializer(data=ads)
        serializer.is_valid(raise_exception=True)
        ads = serializer.save()

        response = AdvertisementSerializer(ads)
        return Response(response.data, status=status.HTTP_201_CREATED)


