from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

from ads.serializers.AdsSerializer import AdvertisementSerializer
from ..repositories.AdsRepository import AdsRepository
from ..models.Image import Image
from ..presentors.AdsPresentor import AdsPresentor


class ParamsController(APIView):

    @staticmethod
    def get(request, pk):
        ads = AdsRepository.get_by_id(pk)
        return Response(AdsPresentor.item(ads), status=status.HTTP_200_OK)

    @staticmethod
    def put(request, pk):
        ads = AdsRepository.get_by_id(pk)
        fields = request.data

        serializer = AdvertisementSerializer(instance=ads, data=fields)
        serializer.is_valid(raise_exception=True)
        ads = serializer.save()

        return Response(AdsPresentor.item(ads), status=status.HTTP_200_OK)

    @staticmethod
    def delete(request, pk):
        AdsRepository.delete(AdsRepository.get_by_id(pk))
        return Response(status=status.HTTP_204_NO_CONTENT)

    @staticmethod
    def patch(request, pk):
        ads = AdsRepository.get_by_id(pk)
        try:
            field = request.data['file']
        except:
            raise Response('вы не передали файл', status=400)
        image = Image.objects.create(data=field)
        ads.image_set.add(image)

        return Response(status=204)
