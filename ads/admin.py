from django.contrib import admin
from .models import Ads, Address, Flat, Image

# Register your models here.
admin.site.register(Ads)
admin.site.register(Address)
admin.site.register(Flat)
admin.site.register(Image)
