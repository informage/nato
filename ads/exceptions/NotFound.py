from rest_framework.exceptions import APIException


class NotFound(APIException):
    status_code = 404
    default_detail = 'не унывайте, загрузите сами такую модель и будет вам счастье'
    default_code = 'екх,ничего не нашлось'
