from rest_framework.exceptions import APIException


class Duplicate(APIException):
    status_code = 400
    default_detail = 'такая модель уже существует'
