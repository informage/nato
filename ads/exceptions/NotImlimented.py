from rest_framework.exceptions import APIException


class NotImplemented(APIException):
    status_code = 418
    default_detail = 'не реализовано'
    default_code = 'я чайник, а не кофемашинка'
