from django.urls import path

from ads.controllers.MainController import MainController
from ads.controllers.PasramsController import ParamsController


app_name = "articles"

# app_name will help us do a reverse look-up latter.
urlpatterns = [
    path('ads/', MainController.as_view()),
    path('ads/<str:pk>/', ParamsController.as_view())
]