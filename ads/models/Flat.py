import uuid
from django.db import models
from .Ads import Ads


class Flat(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    advertisement = models.ForeignKey(Ads, on_delete=models.CASCADE, null=True)
    floor = models.CharField(max_length=50)
    count_rooms = models.IntegerField(null=True)
    area = models.FloatField(null=True)
    build_year = models.IntegerField(null=True)

    def __str__(self):
        return str(self.id)
