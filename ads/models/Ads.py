from django.db import models
import uuid


class Ads(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    cost = models.FloatField()
    comment = models.TextField()
    url = models.URLField(unique=True, null=True)
    date_pub = models.DateField()
    contact_number = models.CharField(max_length=12)

    def __str__(self):
        return str(self.id)
