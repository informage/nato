from django.db import models
from .Ads import Ads
import uuid


class Image(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    data = models.ImageField()
    advertisement = models.ForeignKey(Ads, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return str(self.id)
