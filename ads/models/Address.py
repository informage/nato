import uuid
from django.db import models
from .Flat import Flat


class Address(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    flat = models.ForeignKey(Flat, on_delete=models.CASCADE, null=True)
    country = models.CharField(max_length=255, default="Россия")
    region = models.CharField(max_length=255)
    city = models.CharField(max_length=255)
    district = models.CharField(max_length=255)
    street = models.CharField(max_length=255)
    house = models.CharField(max_length=255)

    def __str__(self):
        return str(self.id)
