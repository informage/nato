from abc import ABC, abstractmethod, abstractclassmethod


class AbstractPresentor(ABC):

    @abstractclassmethod
    def transform(cls, target):
        pass

    @classmethod
    def item(cls, target):
        if not bool(target):
            return None
        return cls.transform(target)

    @classmethod
    def collection(cls, targets):
        return list(map(cls.item, targets))

    @classmethod
    def relation_item(cls, relation, transform):
        try:
            return transform(relation.get())
        except:
            return None
