from .AbstractPresentor import AbstractPresentor
from .AddressPresentor import AddressPresentor


class FlatPresentor(AbstractPresentor):
    @classmethod
    def transform(cls, target):
        return {
            "id": target.id,
            "floor": target.floor,
            "count_rooms": target.count_rooms,
            "area": target.area,
            "build_year": target.build_year,
            "address": cls.relation_item(target.address_set, AddressPresentor.item)
        }
