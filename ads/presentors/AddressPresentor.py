from .AbstractPresentor import AbstractPresentor


class AddressPresentor(AbstractPresentor):
    @classmethod
    def transform(cls, target):
        return {
            'id': target.id,
            'country': target.country,
            'region': target.region,
            'city': target.city,
            'district': target.district,
            'street': target.street,
            'house': target.house,
        }
