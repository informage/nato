from .AbstractPresentor import AbstractPresentor
from .FlatPresentor import FlatPresentor


class AdsPresentor(AbstractPresentor):
    @classmethod
    def transform(cls, target):
        return {
            "id": target.id,
            "cost": target.cost,
            "comment": target.comment,
            "url": target.url,
            "date_pub": target.date_pub,
            "contact_number": target.contact_number,
            "flat": cls.relation_item(target.flat_set, FlatPresentor.item)
        }
