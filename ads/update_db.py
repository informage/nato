from .models import Ads
from datetime import datetime, timedelta


def delete_old():
    Ads.objects.filter(date_pub__lte=datetime.now() - timedelta(days=60)).delete()
