from .AbstractRepository import AbstractRespository
from ads.models import Address


class AddressRepository(AbstractRespository):
    @staticmethod
    def model():
        return Address

    @classmethod
    def create(cls, fields, relation=False):
        if not bool(relation):
            relation = cls.model().objects
        return relation.create(**fields)

    @classmethod
    def update(cls, model, fields):
        model.cost = fields.get('cost', model.cost)
        model.country = fields.get('country', model.country)
        model.region = fields.get('region', model.region)
        model.city = fields.get('city', model.city)
        model.districts = fields.get('district', model.districts)
        model.street = fields.get('street', model.street)
        return model.save()

    @classmethod
    def delete(cls, model):
        return model.delete()
