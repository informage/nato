from abc import ABC, abstractmethod
from ads.exceptions.NotFound import NotFound


class AbstractRespository(ABC):
    @staticmethod
    @abstractmethod
    def model():
        pass

    @classmethod
    def create(cls, fields, relation=False):
        return cls.relation_or_model(relation).create(**fields)

    @classmethod
    def update(cls, model, fields):
        return model.update(fields)

    @classmethod
    def delete(cls, model):
        return model.delete()

    @classmethod
    def update_relation(cls, relation, fields):
        try:
            related_model = relation.get()
        except:
            related_model = False

        if fields is None:
            return bool(relation.clear())

        if not bool(fields):
            return False

        if bool(related_model):
            return cls.update(related_model, fields)

        return cls.create(fields, relation)

    @classmethod
    def get_by_id(cls, pk):
        try:
            return cls.model().objects.get(pk=pk)
        except:
            raise NotFound()

    @classmethod
    def get_by_id_safe(cls, pk):
        try:
            return cls.get_by_id(pk)
        except:
            return False

    @classmethod
    def delete_by_id(cls, pk):
        return cls.delete(cls.get_by_id(pk))

    @classmethod
    def relation_or_model(cls, relation):
        return relation if bool(relation) else cls.model().objects

    def not_empty(*args):
        for arg in args:
            if not bool(arg):
                return False
        return True
