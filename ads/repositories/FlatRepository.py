from .AbstractRepository import AbstractRespository
from .AddressRepository import AddressRepository
from ads.models import Flat


class FlatRepository(AbstractRespository):
    @staticmethod
    def model():
        return Flat

    @classmethod
    def create(cls, fields, relation=False):
        flat = cls.relation_or_model(relation).create(**fields)
        address = fields.get('address')

        if bool(address):
            AddressRepository.create(address, flat.address_set)

        return flat

    @classmethod
    def update(cls, model, fields):
        model.floor = fields.get('floor', model.cost)
        model.count_rooms = fields.get('count_rooms', model.count_rooms)
        model.area = fields.get('area', model.region)
        model.build_year = fields.get('build_year', model.build_year)

        address = fields.get('address', False)
        AddressRepository.update_relation(model.address_set, address)
        model.save()

        return model

    @classmethod
    def delete(cls, model):
        return model.delete()
