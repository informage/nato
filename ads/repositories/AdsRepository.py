from .AbstractRepository import AbstractRespository
from .FlatRepository import FlatRepository
from ads.models import Ads
from ads.exceptions.Duplicate import Duplicate
from ads.exceptions.Oudated import Oudated
from teleAds.models.user import User
from datetime import datetime


class AdsRepository(AbstractRespository):
    @staticmethod
    def model():
        return Ads

    @classmethod
    def create(cls, fields, relation=False):
        cls.validate(fields)

        ads = cls.relation_or_model(relation).create(**fields)
        flat = fields.get('flat')
        if bool(flat):
            FlatRepository.create(flat, ads.flat_set)
            cls.find_users_request(fields)

        cls.send_notification_to_user(fields, ads.id)
        return ads

    @classmethod
    def update(cls, model, fields):
        from ads.presentors.AdsPresentor import AdsPresentor
        old_model = AdsPresentor.item(model)

        model.cost = fields.get('cost', model.cost)
        model.comment = fields.get('comment', model.comment)
        model.url = fields.get('url', model.url)
        model.date_pub = fields.get('date_pub', model.date_pub)
        model.contact_number = fields.get('contact_number', model.contact_number)
        model.save()

        flat = fields.get('flat', False)
        FlatRepository.update_relation(model.flat_set, flat)

        cls.on_update(model, old_model)
        return model

    @classmethod
    def delete(cls, model):
        return model.delete()

    @classmethod
    def is_exist(cls, fields):
        flat = fields.get('flat', {})
        floor = flat.get('floor', '')
        address = flat.get('address', {})
        city = address.get('city', '')
        district = address.get('district', '')
        street = address.get('street', '')
        house = address.get('house', '')
        fill = cls.not_empty(flat, address, city, district, street, house)

        if not fill:
            return False

        res = Ads.objects.filter(
            cost=fields.get('cost'),
            flat__floor=floor,
            flat__address__city=city,
            flat__address__district=district,
            flat__address__street=street,
            flat__address__house=house
        )
        c = list(res)
        return bool(list(res))

    @classmethod
    def find_by_request(cls, request):
        qs = cls.model().objects.filter(
            cost__gte=request.get('min'),
            cost__lte=request.get('max'),
            flat__count_rooms__in=request.get('count_rooms'),
            flat__address__district__in=request.get('districts')
        ).order_by(request.get('sort'))

        if request.get('direction') == 'DESC':
            return qs.reverse()
        return qs

    @classmethod
    def find_users_request(cls, fields):
        flat = fields.get('flat', {})
        address = flat.get('address', {})
        return User.objects.filter(
            request__districts__contains=[address.get('district', 'поставьте 5')],
            request__min__lte=fields.get('cost'),
            request__max__gte=fields.get('cost'),
            request__count_rooms__contains=[flat.get('count_rooms')],
        ).distinct('id').values_list('id', flat=True)

    @classmethod
    def send_notification_to_user(cls, fields, pk, message='появилось новое предложение'):
        from teleAds.bot.index import send_notifications
        ids = list(cls.find_users_request(fields))
        if len(ids) > 0:
            send_notifications(ids[0], message, pk)

    @classmethod
    def on_update(cls, model, old_model):
        from ads.presentors.AdsPresentor import AdsPresentor
        model = AdsPresentor.item(model)
        if model == old_model:
            return
        cls.send_notification_to_user(model, model.get('id'), message='обновилось предложение')

    @classmethod
    def validate(cls, fields):
        date_pub = fields.get('date_pub', datetime.now().date().strftime('%Y-%m-%d'))
        if cls.is_exist(fields):
            raise Duplicate
        if datetime.now().toordinal() - date_pub.toordinal() > 60:
            raise Oudated
