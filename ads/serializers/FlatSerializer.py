from rest_framework import serializers
from ads.exceptions.NotImlimented import NotImplemented
from .AddressSerializer import AddressSerializer


class FlatSerializer(serializers.Serializer):
    id = serializers.UUIDField(read_only=True)
    floor = serializers.CharField(required=False)
    count_rooms = serializers.IntegerField(required=False)
    area = serializers.FloatField(required=False)
    build_year = serializers.IntegerField(allow_null=True, required=False)
    address = AddressSerializer(required=False, allow_null=True)

    def create(self, validated_data):
        raise NotImplemented

    def update(self, instance, validated_data):
        raise NotImplemented
