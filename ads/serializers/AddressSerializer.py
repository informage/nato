from rest_framework import serializers
from ads.exceptions.NotImlimented import NotImplemented


class AddressSerializer(serializers.Serializer):
    id = serializers.UUIDField(read_only=True)
    flat = serializers.UUIDField(required=False)
    country = serializers.CharField(max_length=255, required=False, default='Россия')
    region = serializers.CharField(max_length=255, required=False)
    city = serializers.CharField(max_length=255, required=False)
    district = serializers.CharField(max_length=255, required=False)
    street = serializers.CharField(max_length=255, required=False)
    house = serializers.CharField(max_length=255, required=False)

    def create(self, validated_data):
        raise NotImplemented

    def update(self, instance, validated_data):
        raise NotImplemented
