from rest_framework import serializers
from datetime import datetime
from .FlatSerializer import FlatSerializer
from ads.repositories.AdsRepository import AdsRepository

class AdvertisementSerializer(serializers.Serializer):
    id = serializers.UUIDField(read_only=True)
    cost = serializers.FloatField(required=False, default=0)
    comment = serializers.CharField(required=False, default='комментарии отсутствуют')
    url = serializers.URLField(required=False, allow_null=True)
    date_pub = serializers.DateField(required=False, default=datetime.now().date())
    contact_number = serializers.CharField(max_length=12, required=False)
    flat = FlatSerializer(required=False, allow_null=True)

    def create(self, validated_data):
        return AdsRepository.create(validated_data)

    def update(self, instance, validated_data):
        return AdsRepository.update(instance, validated_data)