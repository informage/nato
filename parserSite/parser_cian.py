from selenium import webdriver
import datetime
from .dictionaries_of_correspondence import correspondence_month1, correspondence_month2
import urllib.request
from selenium.webdriver.chrome.options import Options
import requests
import json
#%%
class ParserCian():
    def __init__(self, link):
        self.link = link


    def get_connection(self):
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        
        self.driver = webdriver.Chrome(options=chrome_options)
        self.driver.get(self.link)


    def get_address(self):
        try:
            full_address = self.driver.find_elements_by_xpath("//*[@id='frontend-offer-card']/main/div[2]/div[1]/section/div/div[1]/div[1]/address/a")
            full_address[0].text == str
            
        except:
            full_address = self.driver.find_elements_by_xpath("//*[@id='frontend-offer-card']/main/div[2]/div[1]/section/div/div[1]/div[2]/address/a")
                                                 
        region = full_address[0].text
        city = full_address[1].text
        district = full_address[2].text.replace('р-н ','')
        
        list_of_street = []
        
        if 'подъезд' in full_address[-1].text:
            for y in full_address[3:-2]:
                list_of_street.append(y.text)
                
            street = ', '.join(list_of_street)
            house = full_address[-2].text
            
        else:
            for y in full_address[3:-1]:
                list_of_street.append(y.text)
                
            street = ', '.join(list_of_street)
            house = full_address[-1].text
            
        return {'region': region, 
                'city': city, 
                'district': district.upper(), 
                'street': street, 
                'house': house}
    
    
    def get_flat(self):
        address = self.get_address()
        
        block_of_parameters = self.driver.find_elements_by_class_name("a10a3f92e9--info-title--2bXM9")
        block_of_values = self.driver.find_elements_by_class_name("a10a3f92e9--info-value--18c8R")
        
        list_of_parameters = []
        list_of_values = []
        
        for j in block_of_parameters:
            list_of_parameters.append(j.text)
            
        for i in block_of_values:
            list_of_values.append(i.text)
            
        for k in range(len(list_of_parameters)):
            if list_of_parameters[k] == 'Этаж':
                floor = list_of_values[k].replace(' из ','/')
                
            elif list_of_parameters[k] == 'Общая':
                area = list_of_values[k].replace(' м²','').replace(',', '.')
                
            elif list_of_parameters[k] == 'Построен':
                year_of_construction_of_the_house = list_of_values[k]
                
        try:
            floor
            
        except:
            floor = None
            
        try:
            area = float(area)
            
        except:
            area = None
            
        try:
            year_of_construction_of_the_house = int(year_of_construction_of_the_house)
            
        except:
            year_of_construction_of_the_house = None
        
        advertisement_header = self.driver.find_element_by_xpath("//*[@id='frontend-offer-card']/main/div[2]/div[1]/section/div/div[1]/h1").text
        
        if advertisement_header[0].isalpha():
            number_of_rooms = 1
            
        else:
            number_of_rooms = int(advertisement_header[0])
        
        
        return {'floor': floor, 
                'countRooms': number_of_rooms, 
                'area': area, 
                'buildYear': year_of_construction_of_the_house, 
                'address': address}
              
    
    def get_ads(self):
        try:
            flat = self.get_flat()
            
            date = self.driver.find_element_by_class_name("a10a3f92e9--container--3nJ0d").text
            
            if date.startswith('сегодня'):
                date_of_publication_of_the_advertisement = datetime.datetime.now().strftime('%Y-%m-%d')
                
            elif date.startswith('вчера'):
                yesterday = datetime.datetime.now() - datetime.timedelta(days=1)
                date_of_publication_of_the_advertisement = yesterday.strftime('%Y-%m-%d')
                
            elif date[0].isdigit():
                if date[1].isdigit():
                    if date[7].isdigit():
                        year = date[7:11]
                        
                    else:
                        year = datetime.datetime.now().strftime('%Y')
    
                    month_of_letters = date[3:6]
                    month = correspondence_month1[month_of_letters]
                    date_of_publication_of_the_advertisement = year + month + date[:2] 
                    
                else:
                    if date[6].isdigit():
                        year = date_of_publication_of_the_advertisement[6:10]
                        
                    else:
                        year = datetime.datetime.now().strftime('%Y')
                        
                    month_of_letters = date[2:5]
                    month = correspondence_month2[month_of_letters]
                    date_of_publication_of_the_advertisement = year + month + date[0] 
                
            comment_of_the_owner_of_the_advertisement = self.driver.find_elements_by_class_name("a10a3f92e9--description-parts--27OsH")[0].text.replace('\n', ' ')
            
            try:
                price = float(self.driver.find_element_by_xpath("//*[@id='frontend-offer-card']/main/div[3]/div/div[1]/div[1]/div[1]/div/div[1]/div/span/span[1]").text.replace(' ₽/мес.','').replace(' ', '').replace(',', '.'))
            
            except:
                price = float(self.driver.find_element_by_xpath("//*[@id='frontend-offer-card']/main/div[3]/div/div[1]/div[1]/div[2]/div/div[1]/div/span/span[1]").text.replace(' ₽/мес.','').replace(' ', '').replace(',', '.'))
            
            try:
                self.driver.find_element_by_xpath("//*[@id='frontend-offer-card']/main/div[3]/div/div[1]/div[1]/div[1]/div/div[2]/div/div[2]/div[1]/button/span").click()
                block_of_phones = self.driver.find_elements_by_xpath("//*[@id='frontend-offer-card']/main/div[3]/div/div[1]/div[1]/div[1]/div/div[2]/div/div[2]/div[1]/div/a")
            
            except:
                self.driver.find_element_by_xpath("//*[@id='frontend-offer-card']/main/div[3]/div/div[1]/div[1]/div[2]/div/div[2]/div/div[2]/div[1]/button/span").click()
                block_of_phones = self.driver.find_elements_by_xpath("//*[@id='frontend-offer-card']/main/div[3]/div/div[1]/div[1]/div[2]/div/div[2]/div/div[2]/div[1]/div/a")
          
            list_of_phones = []
            
            for m in block_of_phones:
                list_of_phones.append(m.text.replace('-', '').replace(' ', ''))
                
            phones_of_the_owner_of_the_advertisement = ', '.join(list_of_phones)
            
            self.driver.quit()
            
            
            return {'cost': price, 
                    'comment': comment_of_the_owner_of_the_advertisement, 
                    'url': self.link, 
                    'datePub': date_of_publication_of_the_advertisement, 
                    'contactNumber': phones_of_the_owner_of_the_advertisement, 
                    'flat': flat}
   
        except:
            return "Объявление снято с публикации"    
        
    def get_photos(self):
        start_json_template = "window._cianConfig['frontend-offer-card'] = "
        
        url_photos = []
        
        response = requests.get(self.link)
        html = response.text
        
        if start_json_template in html:
            start = html.index(start_json_template) + len(start_json_template)
            end = html.index('</script>', start)
            json_raw = html[start:end].strip()[:-1]
            json_ = json.loads(json_raw)
            
            for item in json_:
                if item['key'] == 'defaultState':
                    for photo in item['value']['offerData']['offer']['photos']:
                        url_photos.append(photo['fullUrl'])
                    break
                
            photos = []
            
            for i in url_photos:
                photo = []
                
                name = i.split('/')[-1]
                relative_path = 'resources/' + name
                
                urllib.request.urlretrieve(i, relative_path)
                
                photo.append([name, relative_path])
                photos.append(photo)
            
        return photos
#%%
t = ParserCian('https://chelyabinsk.cian.ru/rent/flat/233276165/')
t.get_connection()
print(t.get_ads())
#%%
class CrawlerCian():
    def __init__(self, link):
        self.link = link
        
        
    def get_connection(self):
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        
        self.driver = webdriver.Chrome(options=chrome_options)
        self.driver.get(self.link)
        
    
    def get_links_from_page(self):
        self.list_of_links_to_flats = []
        flats = self.driver.find_elements_by_xpath("//div[@class = 'c6e8ba5398--info-section--Sfnx- c6e8ba5398--main-info--oWcMk']//a")
        
        for i in flats:
            link = i.get_attribute('href')
            self.list_of_links_to_flats.append(link)
        self.driver.quit() 
        
        return self.list_of_links_to_flats  
#%%
class ScrapperCian():
    def __init__(self):
        self.all_links = []
        
        
    def get_all_links(self, pages = 54):     
        for i in range(1, pages + 1):
            site = CrawlerCian(f"https://chelyabinsk.cian.ru/cat.php?deal_type=rent&engine_version=2&offer_type=flat&p={i}&region=5048&type=4")
            site.get_connection()
            links = site.get_links_from_page()
            
            for i in links:
                self.all_links.append(i)
                
        return self.all_links


    def get_data_from_page(self):
        for i in self.all_links:
            flat = ParserCian(i) 
            flat.get_connection()
            print(flat.get_ads())
            print(flat.get_photos())