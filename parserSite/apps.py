from django.apps import AppConfig


class ParsersiteConfig(AppConfig):
    name = 'parserSite'
