from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import urllib.request


# %%
class ParserDomofond():
    def __init__(self, link):
        self.link = link

    def get_connection(self):
        chrome_options = Options()
        chrome_options.add_argument("--headless")

        self.driver = webdriver.Chrome(options=chrome_options)
        self.driver.get(self.link)

    def get_address(self):
        full_address = self.driver.find_element_by_xpath(
            "//*[@id='root']/main/div/div/div[1]/div[3]/div[1]/div[2]/a").text
        list_of_full_address = full_address.split(', ')

        region = list_of_full_address[-1]
        city = list_of_full_address[-2]
        district = list_of_full_address[-3]

        if list_of_full_address[0] == 'Челябинск':
            start_index = 1

        elif list_of_full_address[1] == 'Челябинск':
            start_index = 2

        else:
            start_index = 0

        if 'улица' in list_of_full_address[-4]:
            list_of_street = list_of_full_address[start_index:-3]
            street = ', '.join(list_of_street)
            house = None

        elif 'проспект' in list_of_full_address[-4]:
            list_of_street = list_of_full_address[start_index:-3]
            street = ', '.join(list_of_street)
            house = None

        elif 'ул' in list_of_full_address[-4]:
            list_of_street = list_of_full_address[start_index:-3]
            street = ', '.join(list_of_street)
            house = None

        elif 'подъезд' in list_of_full_address[-4]:
            list_of_street = list_of_full_address[start_index:-5]
            street = ', '.join(list_of_street)
            house = list_of_full_address[-5]

        else:
            list_of_street = list_of_full_address[start_index:-4]
            street = ', '.join(list_of_street)
            house = list_of_full_address[-4]

        return {'region': region,
                'city': city,
                'district': district.upper(),
                'street': street,
                'house': house}

    def get_flat(self):
        address = self.get_address()

        block_of_parameters = self.driver.find_elements_by_xpath(
            "/html/body/div[1]/main/div/div/div[2]/div[1]/div[7]/div/div/span[1]")
        block_of_values = self.driver.find_elements_by_xpath(
            "/html/body/div[1]/main/div/div/div[2]/div[1]/div[7]/div/div/span[2]")

        list_of_parameters = []
        list_of_values = []

        for j in block_of_parameters:
            list_of_parameters.append(j.text)

        for i in block_of_values:
            list_of_values.append(i.text)

        for k in range(len(list_of_parameters)):
            if list_of_parameters[k] == 'Этаж:':
                floor = list_of_values[k]

            elif list_of_parameters[k] == 'Площадь:':
                area = list_of_values[k].replace(' м²', '')

            elif list_of_parameters[k] == 'Комнаты:':
                if list_of_values[k] == 'Студия':
                    number_of_rooms = 1

                else:
                    number_of_rooms = list_of_values[k]
        try:
            floor

        except:
            floor = None

        try:
            area = float(area)

        except:
            area = None

        try:
            number_of_rooms = int(number_of_rooms)

        except:
            number_of_rooms = None

        if self.driver.find_element_by_xpath(
                "/html/body/div[1]/main/div/div/div[2]/div[1]/div[8]/h5").text == "Информация о доме":
            if self.driver.find_element_by_xpath(
                    "/html/body/div[1]/main/div/div/div[2]/div[1]/div[8]/div/div[1]/span[1]").text == 'Год постройки:':
                year_of_construction_of_the_house = int(self.driver.find_element_by_xpath(
                    "/html/body/div[1]/main/div/div/div[2]/div[1]/div[8]/div/div[1]/span[2]").text)

            else:
                year_of_construction_of_the_house = None

        elif self.driver.find_element_by_xpath(
                "/html/body/div[1]/main/div/div/div[2]/div[1]/div[9]/h5").text == "Информация о доме":
            if self.driver.find_element_by_xpath(
                    "/html/body/div[1]/main/div/div/div[2]/div[1]/div[9]/div/div[1]/span[1]").text == 'Год постройки:':
                year_of_construction_of_the_house = int(self.driver.find_element_by_xpath(
                    "/html/body/div[1]/main/div/div/div[2]/div[1]/div[9]/div/div[1]/span[2]").text)

            else:
                year_of_construction_of_the_house = None

        else:
            year_of_construction_of_the_house = None

        return {'floor': floor,
                'countRooms': number_of_rooms,
                'area': area,
                'buildYear': year_of_construction_of_the_house,
                'address': address}

    def get_phone(self):
        try:
            self.driver.find_element_by_xpath("//*[@id='root']/main/div/div/div[2]/div[2]/button").click()

            block_of_phones = self.driver.find_elements_by_xpath(
                "/html/body/div[1]/main/div/div/div[2]/div[2]/button/a").text

            list_of_phones = []

            for m in block_of_phones:
                list_of_phones.append(m.text.replace('-', '').replace(' ', ''))

            phones_of_the_owner_of_the_advertisement = ', '.join(list_of_phones)

            return phones_of_the_owner_of_the_advertisement
        except:
            return 'оШИБОЧКА'

    def get_ads(self):
        try:
            flat = self.get_flat()

            self.driver.find_element_by_xpath("//*[@id='root']/main/div/div/div[2]/div[2]/button").click()

            try:
                try:
                    phone = self.driver.find_element_by_xpath(
                        "/html/body/div[1]/main/div/div/div[2]/div[2]/button/a").get_attribute('href')
                    phone = phone.split(':')[1]

                except:
                    phone = self.driver.find_element_by_xpath(
                        "/html/body/div[1]/main/div/div/div[2]/div[2]/button/a").text

            except:
                phone = None

            block_of_parameters = self.driver.find_elements_by_xpath(
                "/html/body/div[1]/main/div/div/div[2]/div[1]/div[7]/div/div/span[1]")
            block_of_values = self.driver.find_elements_by_xpath(
                "/html/body/div[1]/main/div/div/div[2]/div[1]/div[7]/div/div/span[2]")

            list_of_parameters = []
            list_of_values = []

            for j in block_of_parameters:
                list_of_parameters.append(j.text)

            for i in block_of_values:
                list_of_values.append(i.text)

            for k in range(len(list_of_parameters)):
                if list_of_parameters[k] == 'Дата публикации объявления:':
                    date = list_of_values[k]

            try:
                if date[1].isdigit():
                    if date[4].isdigit():
                        date_of_publication_of_the_advertisement = date[6:] + '-' + date[3:5] + '-' + date[0:2]

                    else:
                        date_of_publication_of_the_advertisement = date[5:] + '-0' + date[3] + '-' + date[0:2]

                else:
                    day = '-0' + date[0]

                    if date[3].isdigit():
                        date_of_publication_of_the_advertisement = date[5:] + '-' + date[2:4] + day

                    else:
                        date_of_publication_of_the_advertisement = date[4:] + '-0' + date[2] + day
            except:
                date_of_publication_of_the_advertisement = None

            comment_of_the_owner_of_the_advertisement = self.driver.find_element_by_class_name(
                "description__description___2FDOM").text.replace('\n', ' ')

            price = float(
                self.driver.find_element_by_class_name("information__price___2Lpc0").text.replace(' ₽', '').replace(' ',
                                                                                                                    ''))

            self.driver.quit()

            return {'cost': price,
                    'comment': comment_of_the_owner_of_the_advertisement,
                    'url': self.link,
                    'datePub': date_of_publication_of_the_advertisement,
                    'contactNumber': phone,
                    'flat': flat}

        except:
            return self.link

    def get_photos(self):
        try:
            number = self.driver.find_element_by_xpath(
                '/html/body/div[1]/main/div/div/div[2]/div[1]/div[1]/div/span[1]').text.split(' из ')
            number = int(number[1])

            urls = []

            try:

                try:
                    for i in range(6, number + 6):
                        url = self.driver.find_element_by_xpath(
                            f'/html/body/div[1]/main/div/div/div[2]/div[1]/div[2]/div/div/div[2]/div/div[{i}]/div/div/div/img').get_attribute(
                            'src')
                        urls.append(url)
                except:
                    for i in range(1, number + 1):
                        url = self.driver.find_element_by_xpath(
                            f'/html/body/div[1]/main/div/div/div[2]/div[1]/div[3]/div/div/div/div/div[{i}]/div/div/div/img').get_attribute(
                            'src')
                        urls.append(url)
            except:
                for i in range(1, number + 1):
                    url = self.driver.find_element_by_xpath(
                        f'/html/body/div[1]/main/div/div/div[2]/div[1]/div[2]/div/div/div/div/div[{i}]/div/div/div/img').get_attribute(
                        'src')
                    urls.append(url)

            photos = []

            for i in urls:
                photo = []

                name = i.split('/')[-1]
                relative_path = 'resources/' + name

                headers = {
                    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.3'}

                req = urllib.request.Request(url=i, headers=headers)
                html = urllib.request.urlopen(req).read()
                file = open(relative_path, 'wb')
                file.write(html)
                file.close

                photo.append([name, relative_path])
                photos.append(photo)

            return photos
        except:
            return self.link


# %%
class CrawlerDomofond():
    def __init__(self, link):
        self.link = link

    def get_connection(self):
        chrome_options = Options()
        chrome_options.add_argument("--headless")

        self.driver = webdriver.Chrome(options=chrome_options)
        self.driver.get(self.link)

    def get_links_from_page(self):
        self.list_links_flats = []

        flats = self.driver.find_elements_by_xpath("//div[@class = 'search-results__itemCardList___RdWje']//a")

        for i in flats:
            link = i.get_attribute('href')
            self.list_links_flats.append(link)

        self.driver.quit()
        return self.list_links_flats


# %%
class ScrapperDomofond():
    def __init__(self):
        self.all_links = []

    def get_all_links(self, pages=83):
        for i in range(1, pages + 1):
            site = CrawlerDomofond(
                f"https://www.domofond.ru/arenda-kvartiry-chelyabinsk-c2358?RentalRate=Month&Page={i}")
            site.get_connection()
            links = site.get_links_from_page()

            for i in links:
                self.all_links.append(i)

        return self.all_links

    def get_data_from_page(self):
        for i in self.all_links:
            flat = ParserDomofond(i)
            flat.get_connection()
            print(flat.get_ads())
            print(flat.get_photos())


# %%
s = ScrapperDomofond()
d = s.get_all_links(2)
print(d)
# %%
s.get_data_from_page()
