from ads.repositories.AbstractRepository import AbstractRespository
from teleAds.models.request import Request


class RequestRepository(AbstractRespository):
    @staticmethod
    def model():
        return Request

    @classmethod
    def create(cls, fields, relation=False):
        try:
            cls.model().objects.get(**fields)
        except:
            super().create(fields, relation)
