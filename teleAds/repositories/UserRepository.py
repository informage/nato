from ads.repositories.AbstractRepository import AbstractRespository
from teleAds.models.user import User
from .RequestRepository import RequestRepository
from django.conf import settings
from datetime import *
import matplotlib.pyplot as plt


class UserRepository(AbstractRespository):
    @staticmethod
    def model():
        return User

    @classmethod
    def add_request(cls, model, request):
        RequestRepository.create(request, relation=model.request_set)

    @classmethod
    def get_new_user_in_weak(cls):
        (now, week) = cls.get_now_and_week()
        list_dates = list(cls
                          .model()
                          .objects.filter(date_reg__gte=now - week)
                          .order_by('date_reg')
                          .values_list('date_reg', flat=True))
        array_day_in_week = cls.get_array_current_week()
        counts = list(map(list_dates.count, array_day_in_week))
        return array_day_in_week, counts

    @classmethod
    def get_user_in_weak(cls):
        (now, week) = cls.get_now_and_week()
        count_old_user = cls \
            .model() \
            .objects \
            .filter(date_reg__lt=now - week) \
            .count()
        (distinct, counts) = cls.get_new_user_in_weak()
        counts = cls.get_sum_count_user(counts)
        counts = list(map(lambda x: x + count_old_user, counts))
        return distinct, counts

    @classmethod
    def get_user_graph(cls):
        (dates, counts) = cls.get_user_in_weak()
        (dates, counts_new) = cls.get_new_user_in_weak()

        fig, ax = plt.subplots()
        plt.title("Статистика по новым и всем пользователям за неделю", fontweight='bold')
        plt.xlabel("Дата", fontweight='bold')
        plt.ylabel("Количество пользователей", fontweight='bold')
        plt.plot(dates, counts_new, 'o-r', label='Новые')
        plt.plot(dates, counts, 'o-g', label='Старые')
        plt.legend(loc='upper left')
        fig.autofmt_xdate(rotation=25)
        plt.savefig(settings.MEDIA_ROOT + '/график')
        image = open(settings.MEDIA_ROOT + '/график.png', 'rb')
        return image

    @classmethod
    def get_sum_count_user(cls, user_counts):
        for i in range(1, len(user_counts)):
            user_counts[i] += user_counts[i - 1]
        return user_counts

    @classmethod
    def get_array_current_week(cls):
        (now, week) = cls.get_now_and_week()
        distinct = [now - timedelta(days=x) for x in range(0, 7)]
        distinct.reverse()
        return distinct

    @classmethod
    def get_now_and_week(cls):
        return datetime.now().date(), timedelta(days=7)
