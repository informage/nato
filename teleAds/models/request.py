import uuid

from django.db import models
from .user import User
from django.contrib.postgres.fields import ArrayField


class Request(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    min = models.FloatField()
    max = models.FloatField()
    districts = ArrayField(base_field=models.CharField(max_length=50))
    count_rooms = ArrayField(base_field=models.IntegerField())

    def __str__(self):
        return str(self.id)
