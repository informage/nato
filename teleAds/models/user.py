from django.db import models
from datetime import datetime, timedelta


class User(models.Model):
    id = models.CharField(primary_key=True, max_length= 255)
    date_reg = models.DateField(editable=False, default=datetime.now().date())

    def __str__(self):
        return str(self.id)
