import threading
import telebot
from teleAds.repositories.UserRepository import UserRepository
from teleAds.repositories.RequestRepository import RequestRepository
from ads.repositories.AdsRepository import AdsRepository
from .helpers import *
from functools import partial

bot = telebot.TeleBot(None)
thread = None
context = {}
s_m = partial(send_message, bot)


@bot.message_handler(commands=['start'])
def __on_start(message):
    (chat_id, fn, ln) = get_user_info(message)

    if not UserRepository.get_by_id_safe(chat_id):
        UserRepository.create({"id": chat_id})

    markup = get_default_markup(chat_id)
    s_m(chat_id, get_start_message(fn, ln), markup)


@bot.message_handler(regexp=rf'^{FIND_FLAT}$')
def __on_find(message):
    chat_id = message.chat.id
    s_m(chat_id, pre_find_message)
    s_m(chat_id, find_message, gen_in_markup(districts, sep='!'))


@bot.callback_query_handler(only_district)
def __on_pick_region(call):
    chat_id = call.message.chat.id
    s_m(chat_id, pre_pick_region)

    markup = gen_in_markup(districts, call.data, ',', 1)
    markup.row(inline_b('Перейти к выбору количества комнат', callback_data=call.data + '!'))
    s_m(chat_id, pick_region, markup)


@bot.callback_query_handler(with_one_room)
def __on_pick_room(call):
    chat_id = call.message.chat.id
    s_m(chat_id, pre_pick_room)
    s_m(chat_id, pick_room, gen_in_markup(range(1, 7), call.data, i=2))


@bot.callback_query_handler(with_rooms)
def __on_pick_room_repeat(call):
    chat_id = call.message.chat.id
    markup = gen_in_markup(range(1, 7), call.data, ',', 2)
    markup.row(inline_b('Перейти к выбору цены', callback_data=call.data + '!'))
    s_m(chat_id, pick_room_repeat, markup)


@bot.callback_query_handler(on_pick_min)
def __on_pick_min_cost(call):
    chat_id = call.message.chat.id
    context.update([(chat_id, call.data)])
    s_m(chat_id, pick_min_cost)


@bot.message_handler(regexp=rf'^мин:\d+$')
def __on_pick_max_cost(message):
    chat_id = message.chat.id
    try:
        context[chat_id] += message.text[4:] + '!'
        s_m(chat_id, pick_max_cost)
    except:
        context[chat_id] = ''
        s_m(chat_id, 'ты думал,что я тебя не переиграю?')


@bot.message_handler(regexp=rf'^макс:\d+$')
def __on_pick_sort_by(message):
    chat_id = message.chat.id
    try:
        context[chat_id] += message.text[5:] + '!'
        s_m(chat_id, pick_sort_by, gen_in_markup(sorts, context.get(chat_id, ''), size=1, i=5))
    except:
        context[chat_id] = ''
        s_m(chat_id, 'ты думал,что я тебя не переиграю?')


@bot.callback_query_handler(on_sort)
def __on_sort_dir(call):
    chat_id = call.message.chat.id
    s_m(chat_id, pick_sort_dir, gen_in_markup(direction_sort, call.data, '!', 6, 1))


@bot.callback_query_handler(on_results)
def __on_results(call):
    chat_id = call.message.chat.id
    fav = inline_b('Добавить в избранное', callback_data=call.data + '*')
    get = inline_b('Показать результаты', callback_data=call.data + '!')
    s_m(chat_id, results_message, inline_m().row(fav).row(get))


@bot.callback_query_handler(on_fav)
def __on_fav(call):
    chat_id = call.message.chat.id
    get = inline_b('Показать результаты', callback_data=call.data + '!')
    markup = inline_m().row(get)

    request = annotation_to_req(call.data)
    user = UserRepository.get_by_id(chat_id)
    UserRepository.add_request(user, request)
    s_m(chat_id, fav_message, markup)


@bot.callback_query_handler(on_get_all)
def __on_get_all(call):
    chat_id = call.message.chat.id
    request = annotation_to_dict(call.data)
    results = list(AdsRepository.find_by_request(request))
    bot.send_chat_action(call.message.chat.id, action="typing")

    if len(results) > 0:
        result = paginate_ads(results, 1, call.data)
        s_m(chat_id, result[0], result[1])
    else:
        s_m(chat_id, get_all_empty)


@bot.callback_query_handler(on_get_all_next)
def __on_get_all_next(call):
    chat_id = call.message.chat.id
    request = annotation_to_dict(call.data)
    results = list(AdsRepository.find_by_request(request))
    bot.send_chat_action(call.message.chat.id, action="typing")

    if len(results) > 0:
        result = paginate_ads(results, int(call.data.split('?')[1]) + 1, call.data)
        s_m(chat_id, result[0], result[1])

    else:
        s_m(chat_id, get_all_empty)


@bot.callback_query_handler(on_get_one)
def __on_get_one(call):
    chat_id = call.message.chat.id
    ads_id = call.data.split(' ')[1]
    try:
        ads = AdsRepository.get_by_id(ads_id)
        (message, markup) = transform_full(ads)
        s_m(chat_id, message, markup)
        bot.send_chat_action(chat_id, action='upload_photo')

        images = list(map(lambda im: types.InputMediaPhoto(open(im.data.path, 'rb')), ads.image_set.all()))
        if len(images) > 0:
            bot.send_media_group(call.message.chat.id, images)

    except:
        s_m(chat_id, 'квартира не найдена')


@bot.message_handler(regexp=rf'^{ABOUT_BOT}$')
def __about_bot(message):
    s_m(message.chat.id, about_bot)


@bot.message_handler(regexp=rf'^{SUBSCRIBE}$')
def __subscribe_get_all(message, page=1):
    chat_id = message.chat.id
    requests = list(UserRepository.get_by_id(chat_id).request_set.all())

    if len(requests) > 0:
        (text, markup) = paginate_request(requests, page)
        s_m(chat_id, text, markup)
    else:
        s_m(chat_id, get_all_empty)


@bot.callback_query_handler(on_subs_get_next)
def __subscribe_get_next(call):
    __subscribe_get_all(message=call.message, page=int(call.data.split('$')[1]))


@bot.callback_query_handler(on_delete_request)
def __on_delete_request(call):
    chat_id = call.message.chat.id
    array = call.data.split(' ')
    try:
        RequestRepository.delete_by_id(array[1])
        s_m(chat_id, 'был запрос и нет запроса')
    except:
        s_m(chat_id, get_all_empty)
    __subscribe_get_all(message=call.message, page=int(array[2]))


@bot.message_handler(regexp=rf'^{ADMIN}$')
def __on_admin_panel(message):
    markup = gen_in_markup(admin_panel, sep='^', i=0, size=2)
    s_m(message.chat.id, on_admin_panel, markup)


# def __on_create_admin(message)
def send_notifications(uid, message, ads_id):
    markup = types.InlineKeyboardMarkup()
    markup.add(inline_b(text='показать объявление', callback_data=f'объявление {ads_id}'))
    bot.send_message(chat_id=int(uid), text=message, reply_markup=markup)


@bot.callback_query_handler(on_create_user)
def __on_create_admin(call):
    s_m(call.message.chat.id, 'отправьте мне контакт')


@bot.message_handler(content_types=['contact'], func=is_admin)
def __on_send_contact(message):
    chat_id = message.chat.id
    uid = message.contact.user_id
    if uid is None:
        s_m(chat_id, 'у контакта нет номера')
    else:
        admins.append(uid)
        s_m(chat_id, 'успешно')


@bot.callback_query_handler(on_graph)
def __get_graphic(call):
    chat_id = call.message.chat.id
    image = UserRepository.get_user_graph()
    bot.send_photo(chat_id, image)


def run(token, cli=False, on_start=lambda _bot: None):
    global thread

    if thread is None:
        bot.token = token
        thread = threading.Thread(target=bot.polling)
        thread.start()
        on_start(bot)

    return thread
