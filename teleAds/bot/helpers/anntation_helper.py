from .present_helper import transform_to_request
from teleAds.config import sorts_db, districts


def annotation_to_dict(annot):
    array = annot.split('!')
    dict_req = {}

    dict_req.update([('districts', keys_to_value_district(array[1]))]),
    dict_req.update([('count_rooms', keys_to_value_district(array[2], range(1, 7)))]),
    dict_req.update([('min', array[3])]),
    dict_req.update([('max', array[4])]),
    dict_req.update([('sort', sorts_db[int(array[5])])]),
    dict_req.update([('direction', array[6])])

    return transform_to_request(**dict_req)


def annotation_to_req(annot):
    full_req = annotation_to_dict(annot)
    return {
        'districts': full_req.get('districts'),
        'count_rooms': full_req.get('count_rooms'),
        'min': full_req.get('min'),
        'max': full_req.get('max'),
    }


def keys_to_value_district(annot, dictionary=districts):
    return list(map(lambda i: dictionary[int(i)], annot.split(',')))
