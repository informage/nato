from teleAds.config import admins


def only_district(call):
    return len(call.data.split('!')) == 2


def with_one_room(call):
    array = call.data.split('!')
    return len(array) == 3 and len(array[2]) == 0


def with_rooms(call):
    array = call.data.split('!')
    return len(array) == 3 and len(array[2]) != 0


def on_pick_min(call):
    return on_action(call, 4)


def on_sort(call):
    return on_action(call, 6)


def on_results(call):
    return on_action(call, 7) and '*' not in call.data


def on_fav(call):
    return on_action(call, 7) and '*' in call.data


def on_get_all(call):
    return on_action(call, 8) and '?' not in call.data


def on_get_all_next(call):
    return on_action(call, 2, '?')


def on_get_one(call):
    return 'get' in call.data


def on_subs_get_next(call):
    return '$' in call.data


def on_delete_request(call):
    return 'remove' in call.data


def on_action(call, i, sep='!'):
    array = call.data.split(sep)
    return len(array) == i


def on_graph(call):
    return '^0' == call.data


def on_create_user(call):
    return '^1' == call.data


# def on_delete_user(call):
#     return '^2' == call.data


def on_update_db(call):
    return '^2' == call.data


def is_admin(message):
    return message.chat.id in admins
