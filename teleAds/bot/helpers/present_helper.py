from telebot import types

from django.core.paginator import Paginator

from teleAds.config import directions

inline_b = types.InlineKeyboardButton
inline_m = types.InlineKeyboardMarkup


def transform_to_request(districts, count_rooms, min, max, sort, direction):
    if '*' in direction:
        direction = direction.split('*')[0]
    return {
        'districts': districts,
        'count_rooms': count_rooms,
        'min': float(min),
        'max': float(max),
        'sort': sort,
        'direction': directions[int(direction)],
    }


def transform(ads, i):
    flat = ads.flat_set.get()
    address = flat.address_set.get()
    return f'ОБЪЯВЛЕНИЕ НОМЕР: {i + 1}' \
           f'\n\n цена: {str_or_default(ads.cost)}' \
           f'\n дата публикации: {str_or_default(ads.date_pub)}' \
           f'\n район: {str_or_default(address.district)}' \
           f'\n площадь: {str_or_default(flat.area)} \n \n'


def collection(ads_list, page_numb, transformer=transform):
    str = ''
    for i in range(0, len(ads_list)):
        str += transformer(ads_list[i], i + (page_numb - 1) * 4)
    return str


def paginate_ads(ads, page_numb, data):
    markup = types.InlineKeyboardMarkup(row_width=2)
    paginate = Paginator(ads, 4)
    page = paginate.get_page(page_numb)
    message = collection(page, page_numb)
    if page_numb > paginate.num_pages:
        return 'нет такой страницы', None

    if page.has_next():
        if page_numb > 1:
            data = data.split('?')[0]
        markup.row(inline_b(text='следующая страница', callback_data=data + f'?{page_numb}'))

    array = []
    for i in range(0, len(page)):
        array.append(inline_b(text=f'объявление #{i + 1 + (page_numb - 1) * 4}', callback_data=f'get {page[i].id}'))
    markup.add(*array)

    return message, markup


def paginate_request(requests, page_numb):
    markup = types.InlineKeyboardMarkup(row_width=2)
    paginate = Paginator(requests, 4)
    page = paginate.get_page(page_numb)
    message = collection(page, page_numb, transform_request)
    if page_numb > paginate.num_pages:
        return 'нет такой страницы', None
    if page.has_next():
        markup.row(inline_b(text='следующая страница', callback_data=f'${page_numb + 1}'))

    array = []
    for i in range(0, len(page)):
        array.append(
            inline_b(text=f'удалить заявку #{i + 1 + (page_numb - 1) * 4}',
                     callback_data=f'remove {page[i].id} {page_numb}'))
    markup.add(*array)

    return message, markup


def transform_full(ads):
    markup = None
    if ads.url is not None:
        markup = types.InlineKeyboardMarkup(row_width=1)
        markup.add(inline_b(text='Перейти по ссылке', url=ads.url))

    flat = ads.flat_set.get()
    address = flat.address_set.get()
    return f'\n\n цена: {str_or_default(ads.cost)}' \
           f'\n комментарий: {str_or_default(ads.comment)}' \
           f'\n дата публикации: {str_or_default(ads.date_pub)}' \
           f'\n контактный номер: {str_or_default(ads.contact_number)}' \
           f'\n квартира:' \
           f'\n   этаж: {str_or_default(flat.floor)}' \
           f'\n   количество комнат: {str_or_default(flat.count_rooms)}' \
           f'\n   площадь: {str_or_default(flat.area)}' \
           f'\n   год стройки: {str_or_default(flat.build_year)}' \
           f'\n     адрес:' \
           f'\n         страна: {str_or_default(address.country)}' \
           f'\n         регион: {str_or_default(address.region)}' \
           f'\n         город: {str_or_default(address.city)}' \
           f'\n         улица: {str_or_default(address.street)}' \
           f'\n         дом: {str_or_default(address.house)}', markup


def str_or_default(str):
    return 'не указано' if str is None or str == '' else str


def transform_request(request, i):
    return f'запрос №{i + 1}' \
           f'\n\n мин: {request.min}' \
           f'\n макс: {request.max}' \
           f'\n количество комнат: {request.count_rooms}' \
           f'\n районы: {request.districts} \n \n'
