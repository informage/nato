def get_start_message(first_name, last_name):
    return f'Добро пожаловать, {last_name}, в чат-бот для поиска ' \
           f'квартиры с длительной арендой!'


def get_user_info(message):
    user = message.from_user
    return user.id, user.last_name, user.first_name


def send_message(bot, chat_id, text, markup=None):
    bot.send_message(chat_id, text, reply_markup=markup)
