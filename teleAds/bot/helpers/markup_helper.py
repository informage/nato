from teleAds.config import *
from telebot import types


def get_default_markup(uid='2323232323z'):
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup.row(FIND_FLAT).row(HELP, SUBSCRIBE).row(ABOUT_BOT)
    if uid in admins:
        markup.add(ADMIN)
    return markup


def gen_in_markup(keys, data='', sep='', i=0, size=2, old_sep='!'):
    data += sep
    markup = types.InlineKeyboardMarkup(row_width=size)
    markup.add(*generate_inline_keys(keys, data, i, old_sep))
    return markup


def generate_inline_keys(keys, data, index, old_sep):
    result = []
    old_keys = []
    old_keys_str = data.split(old_sep)[index]

    if keys is not None:
        old_keys = list(filter(None, old_keys_str.split(',')))
    for i in range(len(keys)):
        if str(i) in old_keys:
            continue
        result.append(generate_inline_key(i, keys[i], data))

    return result


def generate_inline_key(i, v, data):
    return types.InlineKeyboardButton(text=v, callback_data=data + str(i))

