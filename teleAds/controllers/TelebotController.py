from rest_framework.response import Response
from rest_framework.views import APIView
from teleAds.config import *
from teleAds.bot.index import run as run_telegram_bot


class TelebotController(APIView):
    @staticmethod
    def post(request):
        run_telegram_bot(token)
        return Response('Запущено')


